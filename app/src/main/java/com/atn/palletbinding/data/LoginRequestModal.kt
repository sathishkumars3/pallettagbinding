package com.atn.palletbinding.data

data class LoginRequestModal(val username:String, val password: String)