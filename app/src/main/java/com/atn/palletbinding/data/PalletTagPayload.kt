package com.atn.palletbinding.data

data class PalletTagPayload(val module:String, val palletTagNumber:String, val palletNumber: String,
            val locationTagNumber: String, val locationName:String,val eventType: String,
            val timestamp: Number)