package com.atn.palletbinding.data

data class LoginUserInformation(val additionalInfo: AdditionalInformation, val email: String, val authority:String, val firstName: String, val lastName: String, val name: String)

data class AdditionalInformation(val description: String, val lastLoginTs: String, val homeDashboardId: String, val defaultDashboardId: String, val failedLoginAttempts: Number, val userCredentialsEnabled: Boolean, val homeDashboardHideToolbar: Boolean, val defaultDashboardFullscreen: Boolean)
