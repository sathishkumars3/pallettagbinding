package com.atn.palletbinding.data

data class PalletTagsModalInfo(val id: String, val readTagNumber:String, val readTagName: String, val readTime: String)