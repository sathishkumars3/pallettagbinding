package com.atn.palletbinding.data
data class  TokenModel(
    val token: String,
    val refreshToken: String
)

data class FailedResponse(val status: String, val message: String, val errorCode: String)