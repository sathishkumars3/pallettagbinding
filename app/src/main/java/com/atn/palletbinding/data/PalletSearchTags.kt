package com.atn.palletbinding.data

data class PalletSearchTags(val locationId:String, val name:String, val status: Number, val message: String, val errorCode:Number)