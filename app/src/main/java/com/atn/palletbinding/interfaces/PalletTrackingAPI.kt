package com.atn.palletbinding.interfaces

import com.atn.palletbinding.data.BindRequestModal
import com.atn.palletbinding.data.LoginRequestModal
import com.atn.palletbinding.data.LoginUserInformation
import com.atn.palletbinding.data.PalletSearchTags
import com.atn.palletbinding.data.PalletTagsModalInfo
import com.atn.palletbinding.data.TokenModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface  PalletBindingAPI {
    @POST("/api/auth/login")
    suspend fun getToken(@Body request: LoginRequestModal): Response<TokenModel>

    @GET("/api/auth/user")
    suspend fun getUserDetails(@Header("Authorization") token: String):Response<LoginUserInformation>

    @POST("/api/auth/logout")
    suspend fun logOutUser(@Header("Authorization") token: String): Response<Unit>

    @GET("api/tenant/tagInfos")
    suspend fun getAvailableTagsInfo(@Query("pageSize") size:String, @Query("page") page: String): Response<Unit>

    @GET("api/device/readPalletTags")
    suspend fun getPalletTags(@Query("deviceNumber") deviceNumber: String, @Header("Authorization") token: String): Response<ArrayList<PalletTagsModalInfo>>

    @GET("api/device/readLocationTags")
    suspend fun getLocationTags(@Query("deviceNumber") deviceNumber: String, @Header("Authorization") token: String):Response<ArrayList<PalletTagsModalInfo>>

    @GET("api/pallettrack/getLatestLocationByPalletNumber")
    suspend fun getPalletSearchTags(@Query("palletNumber") palletNumber:String, @Header("Authorization") token: String): Response<PalletSearchTags>
    
    @POST("api/ptbind/qRBind")
    suspend fun bindQRCode(@Body request: BindRequestModal, @Header("Authorization") token: String):Response<Unit>
}
