package com.atn.palletbinding.interfaces

import com.atn.palletbinding.utils.AppConstants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {
    private val baseUrl = AppConstants.HTTP_SERVER_URL// "http://4.240.70.87:8700/"

    fun getInstance(): Retrofit {
        return  Retrofit.Builder().baseUrl(baseUrl).
        addConverterFactory(GsonConverterFactory.create()).
        build()
    }

}
