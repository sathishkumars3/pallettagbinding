package com.atn.palletbinding.utils

import android.annotation.SuppressLint
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence

class MQTTConnection {

    private var mqttClient: MqttClient? = null
    private var mqttPersistence: MemoryPersistence? = null
    private var mqttConnection: MQTTConnection? = null

    @SuppressLint("NotConstructor")
    private fun MQTTConnection(): MQTTConnection? {
        mqttInit()
        mqttConnect()
        return this.mqttConnection;
    }

    private fun mqttInit() {
        val serverUrl: String = AppConstants.MQTT_URL
        val mqttClientId: String = AppConstants.MQTT_ACCESS_TOKEN
        mqttPersistence = MemoryPersistence()
        try {
            mqttClient = MqttClient(serverUrl, mqttClientId, mqttPersistence)
        } catch (e: MqttException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }

    private fun mqttConnect() {
        if (!mqttClient!!.isConnected) {
            val options = MqttConnectOptions()
            options.connectionTimeout = 60
            options.keepAliveInterval = 60
            val mqttUser: String = AppConstants.MQTT_ACCESS_TOKEN
            options.userName = mqttUser
            options.isCleanSession = true
            options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1
            println("starting connection the server...")
            try {
                mqttClient!!.connect(options)
            } catch (e: MqttException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            println("connected!")
        } else {
            println("Already connected!")
        }
    }

    fun mqttClose() {
        try {
            mqttClient!!.disconnect()
            mqttClient!!.close()
            println("disconnected!")
        } catch (e: MqttException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }

    @Synchronized
    fun getInstance(): MQTTConnection? {
        if (mqttConnection == null) {
            mqttConnection = MQTTConnection()
        }
        return mqttConnection
    }

    fun getMqttClient(): MqttClient? {
        return mqttClient
    }

//    fun connect(context: Context){
//        val serverURI = AppConstants.MQTT_URL //"tcp://4.240.70.87:1883"
//        val accessToken = AppConstants.MQTT_ACCESS_TOKEN //"sJ6ovLEsyrMfLZj4jmU7"
//        val mqttClientPersistence: MqttClientPersistence = MemoryPersistence()
//
//        mqttAndroidClient = MqttAndroidClient(context, serverURI, accessToken, mqttClientPersistence)
//
//        mqttAndroidClient!!.setCallback(object: MqttCallback {
//            override fun messageArrived(topic: String?, message: MqttMessage?) {
//                Log.d("TAG", "Message :${message.toString()} from topic: $topic")
//            }
//
//            override fun connectionLost(cause: Throwable?) {
//                Log.d("TAG", "Connection lost :${cause.toString()}")
//            }
//
//            override fun deliveryComplete(token: IMqttDeliveryToken?) {
//                Log.d("TAG", "DeliveryComplete")
//            }
//        })
//
//        val options = MqttConnectOptions()
//        options.userName = accessToken //"sJ6ovLEsyrMfLZj4jmU7"
//        options.isAutomaticReconnect = true
//        options.isCleanSession = true
//        options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1
//        try{
//            mqttAndroidClient!!.connect(options, null, object: IMqttActionListener {
//                override fun onSuccess(asyncActionToken: IMqttToken?) {
//                    Log.d("TAG", "Connection Success:$asyncActionToken")
////                    publishMessage(topic, payload, 2, true)
////                    Toast.makeText(applicationContext, "Successfully completed", Toast.LENGTH_SHORT).show()
////                    finish()
//                }
//
//                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
//                    Log.d("TAG", "MQTT Connection Failure")
//                }
//            })
//
//        }catch (e: Exception){
//            e.printStackTrace()
//        }
//    }

//    private var mqttAndroidClient: MqttAndroidClient? = null
//
//    constructor(mqttAndroidClient: MqttAndroidClient?) {
//        this.mqttAndroidClient = mqttAndroidClient
//    }
//
//    var mqttConnection = MQTTConnection()
//
//    //    @Synchronized
//    fun getInstance(): MQTTConnection? {
//
//        if (mqttConnection == null) {
//            mqttConnection = MQTTConnection()
//        }
//        return mqttConnection
//    }

//    private fun MQTTConnection(): MQTTConnection {
//        connect(contet)
//
//    }
//
//    fun getMqttClient(): MqttClient? {
//        return mqttClient
//    }
//
//    fun publishMessage(topic: String, msg: String, qos: Int =1, retained: Boolean = false){
//        try{
//            val message = MqttMessage()
//            message.payload = msg.toByteArray()
//            message.qos = qos
//            message.isRetained = retained
//            mqttClient.publish(topic, message, null, object: IMqttActionListener {
//                override fun onSuccess(asyncActionToken: IMqttToken?) {
//                    Log.d("TAG", "$msg published to $topic")
//
//                }
//
//                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
//                    Log.d("TAG", "Failed to publish $msg to $topic")
//                }
//            })
//
//        }catch (e: MqttException){
//            e.printStackTrace()
//        }
//    }
}