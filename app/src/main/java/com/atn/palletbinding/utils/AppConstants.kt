package com.atn.palletbinding.utils

public object AppConstants {
    var HTTP_SERVER_URL = "http://4.240.70.87:8700"
    public var LOGIN_URL = "$HTTP_SERVER_URL/api/auth/login"
    public var IS_UPDATED_TAGS_AVAILABLE_URL = "$HTTP_SERVER_URL/api/isUpdatedTagsAvailable"
    public var UPDATED_PTBINDTAGLIST_URL = "$HTTP_SERVER_URL/api/ptbind/bindTags"

    public var MQTT_URL = "tcp://4.240.70.87:1883"
    public var MQTT_ACCESS_TOKEN = "sJ6ovLEsyrMfLZj4jmU7" //ReadTag sJ6ovLEsyrMfLZj4jmU7
    public var MQTT_TELEMETRY_TOPIC = "v1/devices/me/telemetry"
    public var DEVICE_NUMBER = "5D83A9"
}