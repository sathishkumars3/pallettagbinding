package com.atn.palletbinding.screens

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.ComponentActivity
import com.atn.palletbinding.interfaces.PalletBindingAPI
import com.atn.palletbinding.interfaces.RetrofitHelper
import com.atn.palletbinding.R

class MainScreen : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_screen)
        val logOut = findViewById<TextView>(R.id.btnLogout)
        val extrasInfo = intent.extras

        val btnPalletTagsBinding = findViewById<Button>(R.id.btnBindTags)
//        welcomeLabel.text = "Welcome to : "+ extrasInfo?.getString("Name")

        val retrofitAPI = RetrofitHelper.getInstance().create(PalletBindingAPI::class.java)
        val prefInfo = getSharedPreferences("Pallet_binding", MODE_PRIVATE)
        val token = prefInfo.getString("Token", "")
        logOut.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, LoginScreen::class.java)
            startActivity(intent)
        })

        btnPalletTagsBinding.setOnClickListener {
            val bindScreen = Intent(applicationContext, PalletTagsBinding::class.java)
            startActivity(bindScreen)
        }

    }
}
