package com.atn.palletbinding.screens

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.ComponentActivity
import com.atn.palletbinding.interfaces.PalletBindingAPI
import com.atn.palletbinding.R
import com.atn.palletbinding.interfaces.RetrofitHelper
import com.atn.palletbinding.data.LoginRequestModal
import com.atn.palletbinding.utils.NetworkUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.HttpURLConnection

class LoginScreen : ComponentActivity() {
    val retrofitAPI = RetrofitHelper.getInstance().create(PalletBindingAPI::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.login_screen)
        val btn = findViewById<Button>(R.id.btnLogin)
        val userName = findViewById<EditText>(R.id.etUserName)
        val password = findViewById<EditText>(R.id.etPassword)
        val sharedPrefrences = this.getSharedPreferences("Pallet_binding", MODE_PRIVATE)

        btn.setOnClickListener(View.OnClickListener {
            // val responseInfo =  retrofitAPI.getToken(LoginInformation("satishtvs@gmail.com", "Satish4321"))
            if (!NetworkUtils.isNetworkAvailable(applicationContext)) {
                Toast.makeText(
                    applicationContext,
                    "No Internet Connection. Please connect to Intent.",
                    Toast.LENGTH_SHORT
                ).show()
                return@OnClickListener
            }
            val userName = userName.text.toString()
            val password = password.text.toString()
            if (userName.isEmpty()) {
                Toast.makeText(applicationContext, "Enter Email Id", Toast.LENGTH_SHORT).show()
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(userName).matches()) {
                Toast.makeText(applicationContext, "Enter valid EmailId", Toast.LENGTH_SHORT).show()
            } else if (password.isEmpty()) {
                Toast.makeText(applicationContext, "Enter Password", Toast.LENGTH_SHORT).show()
            } else {
                validateUserDetails(userName, password, sharedPrefrences)
            }
        })

    }

    private fun validateUserDetails(
        userName: String,
        password: String,
        sharedPrefrences: SharedPreferences
    ) {
        GlobalScope.launch(Dispatchers.Main) {
            val result = retrofitAPI.getToken(LoginRequestModal(userName, password))

            if (result.code() == HttpURLConnection.HTTP_OK) {
                Log.d("TAG", result.body().toString())
                val authUserInfo = retrofitAPI.getUserDetails("Bearer " + result.body()?.token)
                if (authUserInfo.code() == HttpURLConnection.HTTP_OK) {
                    Log.d("Response", authUserInfo.body().toString())
                    val myPalletPrefInfo = sharedPrefrences.edit()
                    myPalletPrefInfo.putString("Token", result.body()?.token)
                    myPalletPrefInfo.putString("RefreshToken", result.body()?.refreshToken)
                    myPalletPrefInfo.putBoolean("isLoggedIn", true)
                    val loggedInName =
                        authUserInfo.body()?.firstName.toString() + " " + authUserInfo.body()?.lastName.toString()
                    myPalletPrefInfo.putString("Name", loggedInName)
                    myPalletPrefInfo.commit()
//                    val mainScreen = Intent(applicationContext, MainScreen::class.java)
//                    mainScreen.putExtra("Name", loggedInName)
//                    startActivity(mainScreen)
                    val palletTagBindingScreen = Intent(applicationContext, PalletTagsBinding::class.java)
                    palletTagBindingScreen.putExtra("Name", loggedInName)
                    startActivity(palletTagBindingScreen)
                    finish();
                }
            } else {
                Log.d("TAG", "Invalid Credentials")
                Toast.makeText(applicationContext, "Invalid Credentials", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
