package com.atn.palletbinding.screens

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.ComponentActivity
import com.atn.palletbinding.R
import com.atn.palletbinding.interfaces.RetrofitHelper
import com.atn.palletbinding.data.BindRequestModal
import com.atn.palletbinding.interfaces.PalletBindingAPI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.HttpURLConnection

class PalletTagsBinding : ComponentActivity() {

    private lateinit var btnScanPalletNumber: Button
    private lateinit var btnScanRFIDTag: Button
    private lateinit var btnBindTags: Button
    private lateinit var lblPalletQrString: TextView
    private lateinit var lblRFIDTagQrString: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bind_pallet_tags)
        val retrofitAPI = RetrofitHelper.getInstance().create(PalletBindingAPI::class.java)
        val prefInfo = getSharedPreferences("Pallet_binding", MODE_PRIVATE)
        val token = prefInfo.getString("Token", "")

        btnScanPalletNumber = findViewById(R.id.btnScanPalletNumber)
        btnScanRFIDTag = findViewById(R.id.btnScanRFIDTag)
        btnBindTags = findViewById(R.id.btnBindTagCodes)
        lblPalletQrString = findViewById<TextView>(R.id.lblPalletNumber)
        lblRFIDTagQrString = findViewById<TextView>(R.id.lblRFIDTag)
        val btnLogout = findViewById<Button>(R.id.btnLogout)
        btnLogout.setOnClickListener {
            val intent = Intent(applicationContext, LoginScreen::class.java)
            startActivity(intent)
        }
        btnScanPalletNumber.setOnClickListener {
            val scanButton = Intent(applicationContext, ScanBarcode::class.java)
            startActivityForResult(scanButton, 1002)
        }
        btnScanRFIDTag.setOnClickListener {
            val scanButton = Intent(applicationContext, ScanBarcode::class.java)
            startActivityForResult(scanButton, 1003)
        }

        btnBindTags.setOnClickListener {
            val palletQrCode = lblPalletQrString.text.toString()
            val rfidQRCode = lblRFIDTagQrString.text.toString()
            GlobalScope.launch(Dispatchers.Main) {
                val result = retrofitAPI.bindQRCode(
                    BindRequestModal(rfidQRCode, palletQrCode),
                    "Bearer $token"
                )
                if (result.code() == HttpURLConnection.HTTP_OK) {
                    Toast.makeText(
                        applicationContext,
                        "Successfully bind the QR Codes",
                        Toast.LENGTH_SHORT
                    ).show()
                    lblPalletQrString.text = ""
                    lblRFIDTagQrString.text = ""
                    val alertDialog = AlertDialog.Builder(this@PalletTagsBinding)
                    alertDialog.setMessage("Pallet is successfully bound to Tag")
                    alertDialog.setPositiveButton("Ok") { dialog, _ -> dialog.dismiss() }
                    alertDialog.create().show()

                } else {
                    Toast.makeText(
                        applicationContext,
                        "Unable to save the tags details" + result.body().toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("HTTP-ERROR", result.toString())
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1002 && resultCode == RESULT_OK) {
            lblPalletQrString.text = data?.getStringExtra("Code")
        } else if (requestCode == 1003 && resultCode == RESULT_OK) {
            lblRFIDTagQrString.text = data?.getStringExtra("Code")
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
