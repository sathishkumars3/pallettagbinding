package com.atn.palletbinding.screens

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.ComponentActivity
import com.atn.palletbinding.R

class SplashActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_layout)
        val preferences = getSharedPreferences("Pallet_binding", MODE_PRIVATE)
        val isLoggedInUser = preferences.getBoolean("isLoggedIn", false)
        val logInName = preferences.getString("Name", "")
        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(this, LoginScreen::class.java)
            startActivity(intent)
            finish()
//            if(isLoggedInUser){
//                val intent = Intent(this, MainScreen::class.java)
//                intent.putExtra("Name", logInName)
//                startActivity(intent)
//                finish()
//            }else {
//
//            }
        }, 3000)

    }
}
